import React, { useEffect, useState } from "react";
import logo from '../logo.svg';
import '../App.css';
import NavBar from "../components/navbar";
import { Box, Typography, Select, MenuItem, Button, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, TextField, Card, CardContent, CardActions, Grid } from '@mui/material';
import { fetchTicketData, patchTicketData, postTicketData } from '../api/api';
import { ClassNames } from "@emotion/react";

interface ListItem {
  id: string;
  title: string;
  description: string;
  contact: string;
  information: string;
  created_timestamp: string;
  latest_ticket_update_timestamp: string;
  status: string;
}

const HomePages: React.FC = () => {
  const [data, setData] = useState<ListItem[]>([]);
  const [changedRows, setChangedRows] = useState<ListItem[]>([]);
  const [errorDialogOpen, setErrorDialogOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [filters, setFilters] = useState<string>('All');
  const [order, setOrder] = useState<string>('latest_ticket_update_timestamp');
  const [openDialog, setOpenDialog] = useState(false);
  const [editState, setEditState] = useState(false)
  const [inputData, setInputData] = useState({
    title: '',
    description: '',
    contact: '',
    information: ''
  });

  useEffect(() => {
    getTicket(filters, order); 
  }, []);

  const getTicket = (data: string, order: string) => {
    fetchTicketData(data, order)
      .then((response) => {
        setData(response.data.result.list.map((item: ListItem) => ({
          ...item,
          created_timestamp: new Date(item.created_timestamp).toLocaleDateString(),
          latest_ticket_update_timestamp: new Date(item.latest_ticket_update_timestamp).toLocaleDateString(),
        })));
      })
      .catch((error) => {
        console.error('Error fetching ticket data:', error);
        handleOpenErrorDialog('Error fetching ticket data. Please try again.');
      });
  }

  const updateStatus = (itemId: string, newStatus: string) => {
    setEditState(true)
    const updatedData = data.map((item) => {
      if (item.id === itemId) {
        const updatedItem = {
          ...item,
          status: newStatus,
        };
        setChangedRows((prevChangedRows) => {
          const existingRow = prevChangedRows.find((row) => row.id === itemId);
          if (existingRow) {
            return prevChangedRows.map((row) => (row.id === itemId ? updatedItem : row));
          } else {
            return [...prevChangedRows, updatedItem];
          }
        });
        return updatedItem;
      }
      return item;
    });

    setData(updatedData);
  };

  const handlePatchChanges = () => {
    changedRows.forEach((changedItem) => {
      patchTicketData(changedRows)
        .then((response) => {
          console.log('Data patched successfully:', response.data);
          setEditState(false)
          getTicket(filters, order);
        })
        .catch((error) => {
          console.error('Error patching data:', error);
          handleOpenErrorDialog('Error patching data. Please try again.');
        }
        );
        setEditState(false)
    });
    setChangedRows([]);
  };

  const handleOpenErrorDialog = (message: string) => {
    setErrorMessage(message);
    setErrorDialogOpen(true);
  };

  const handleCloseErrorDialog = () => {
    setErrorDialogOpen(false);
  };

  const handleOpenDialog = () => {
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputData({
      ...inputData,
      [e.target.name]: e.target.value
    });
  };

  const handleSaveData = () => {
    postTicketData(inputData)
      .then((response) => {
        console.log('Data saved successfully:', response.data);
        getTicket(filters, order);
        handleCloseDialog();
      })
      .catch((error) => {
        console.error('Error saving data:', error);
        handleOpenErrorDialog('Error saving data. Please try again.');
      });
    handleCloseDialog();
  };

  const FilterGet = (data: string) => {
    setFilters(data)
    getTicket(data, order)
    console.log(filters)
  }

  const OrderGet = (data: string) => {
    setOrder(data)
    getTicket(filters, data)
    console.log(filters)
  }

  return (
    <div>
      <Box sx={{ border: '1px solid #ccc', borderRadius: '4px', padding: '16px', marginTop: 20, marginLeft: 10, marginRight: 10, }} >
      <Grid container spacing={1}>
        <Grid item xs={6}>
          <Box sx={{ display: 'flex', justifyContent: 'flex-start' }}>
            {editState === false ? (
              <Button variant="contained" color="primary" onClick={handlePatchChanges} disabled={true}>
                Save Changes
              </Button>
            ) : (
              <Button variant="contained" color="primary" onClick={handlePatchChanges} disabled={false}>
                Save Changes
              </Button>
            )}
            <Button variant="contained" color="primary" sx={{ marginLeft: 1 }} onClick={handleOpenDialog}>
              Add Data
            </Button>
          </Box>
        </Grid>
        <Grid item xs={6}>
          <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Select value={order} onChange={(e) => OrderGet(e.target.value as string)}>
                <MenuItem value="latest_ticket_update_timestamp">Update Time</MenuItem>
                <MenuItem value="status">Status</MenuItem>
              </Select>
              <Select value={filters} onChange={(e) => FilterGet(e.target.value as string)}>
                <MenuItem value="All">All</MenuItem>
                <MenuItem value="pending">Pending</MenuItem>
                <MenuItem value="accepted">Accepted</MenuItem>
                <MenuItem value="resolved">Resolved</MenuItem>
                <MenuItem value="rejected">Rejected</MenuItem>
              </Select>
          </Box>
        </Grid>
      </Grid>


        <Box sx={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', gap: '16px', alignItems: 'center' }}>
          {data.map((item) => (
            <Card key={item.id} sx={{ minWidth: 275 }}>
              <CardContent>
                <Typography variant="h6" component="div">
                  {item.title}
                </Typography>
                <Typography sx={{ mb: 0.5 }} color="text.secondary">
                  {item.id}
                </Typography>
                <Typography variant="body2">description : {item.description}</Typography>
                <Typography variant="body2">contact : {item.contact}</Typography>
                <Typography variant="body2">information : {item.information}</Typography>
                <Typography variant="body2">created : {item.created_timestamp}</Typography>
                <Typography variant="body2">updated : {item.latest_ticket_update_timestamp}</Typography>
                <CardActions>
                  <Select
                    value={item.status}
                    onChange={(e) => updateStatus(item.id, e.target.value as string)}
                  >
                    <MenuItem value="pending">Pending</MenuItem>
                    <MenuItem value="accepted">Accepted</MenuItem>
                    <MenuItem value="resolved">Resolved</MenuItem>
                    <MenuItem value="rejected">Rejected</MenuItem>
                  </Select>
              </CardActions>
              </CardContent>
            </Card>
          ))}
        </Box>
      </Box>

      <Dialog open={openDialog} onClose={handleCloseDialog}>
        <DialogTitle>Add Data</DialogTitle>
        <DialogContent>
          <DialogContentText>Enter the details:</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            name="title"
            label="Title"
            type="text"
            fullWidth
            value={inputData.title}
            onChange={handleInputChange}
          />
          <TextField
            margin="dense"
            name="description"
            label="Description"
            type="text"
            fullWidth
            value={inputData.description}
            onChange={handleInputChange}
          />
          <TextField
            margin="dense"
            name="contact"
            label="Contact"
            type="text"
            fullWidth
            value={inputData.contact}
            onChange={handleInputChange}
          />
          <TextField
            margin="dense"
            name="information"
            label="Information"
            type="text"
            fullWidth
            value={inputData.information}
            onChange={handleInputChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDialog} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSaveData} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog open={errorDialogOpen} onClose={handleCloseErrorDialog}>
        <DialogTitle>Error</DialogTitle>
        <DialogContent>
          <DialogContentText>{errorMessage}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseErrorDialog} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default HomePages;
