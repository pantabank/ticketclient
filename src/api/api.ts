import axios from 'axios';

const baseURL = 'http://localhost:5000';

export const fetchTicketData = (filter: any, order: any) => {
  return axios.get(`${baseURL}/v1/ticket/?status=${filter}&order=${order}`);
};

export const patchTicketData = (newData: any) => {
  return axios.patch(`${baseURL}/v1/ticket/`, newData);
};

export const postTicketData = (newData: any) => {
  return axios.post(`${baseURL}/v1/ticket/`, newData);
};