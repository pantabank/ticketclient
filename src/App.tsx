import React from 'react';
import logo from './logo.svg';
import './App.css';
import HomePages from './pages/home'
import NavBar from './components/navbar';

function App() {
  return (
    <div>
      <HomePages/>
      <NavBar/>
    </div>
  );
}

export default App;
