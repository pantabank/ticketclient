import React from "react";
import logo from '../logo.svg';
import '../styles.css';

const NavBar = () => {
  return (
    <div className="sticky-element">
      <nav>
        <ul className="menu">
          <li>
            <a>Ticket Manager</a>
          </li>
        </ul>
      </nav>
    </div>
  );
}

export default NavBar;
