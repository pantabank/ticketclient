import React, { ChangeEvent, useState } from 'react';
import { Select, MenuItem, CardActions } from '@mui/material';

interface FilterProps {
    onValueChange: (value: string) => void;
}

const Filter:React.FC<FilterProps> = ({ onValueChange }) => {

    const [value, setValue] = useState('');

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const newValue = event.target.value;
        setValue(newValue);
        onValueChange(newValue);
    };

    return (
        <div>
            <CardActions>
                  <Select value={value} onChange={() => handleChange}>
                    <MenuItem value="pending">Pending</MenuItem>
                    <MenuItem value="accepted">Accepted</MenuItem>
                    <MenuItem value="resolved">Resolved</MenuItem>
                    <MenuItem value="rejected">Rejected</MenuItem>
                  </Select>
              </CardActions>
        </div>
    )
}

export default Filter