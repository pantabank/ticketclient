import React from 'react';

interface ListItem {
   id: string;
   title: string;
   description: string;
   contact: string;
   information: string;
   created_timestamp: string;
   latest_ticket_update_timestamp: string;
   status: string
}

interface ListProps {
  items: ListItem[];
}

const List: React.FC<ListProps> = ({ items }) => {
  return (
    <div className="sticky-element">
      {items.map((item) => (
        <li key={item.id}>{item.id}{item.description}</li>
      ))}
    </div>
  );
};

export default List;